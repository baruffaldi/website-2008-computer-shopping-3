<?php
/**
 * Index
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage ShortInternetLink
 */

try { require dirname( __FILE__ ) . '/application/bootstrap.php'; }
 
catch ( Exception $exception ) 
{

/**
 * @todo write a smarty template to get this courtesy message
 */
    echo '<html><body><center>'
       . 'An exception occured while bootstrapping the application.';
       
    if ($_SITE['config']['env']['type'] != 'production' ) 
        echo '<br /><br />' . $exception->getMessage( ) . '<br />'
           . '<div align="left">Stack Trace:' 
           . '<pre>' . $exception->getTraceAsString( ) . '</pre></div>';
    
    echo '</center></body></html>';
    exit( 1 );
}

// *** Let's output!
$_SITE['frontController']['handler']->dispatch();