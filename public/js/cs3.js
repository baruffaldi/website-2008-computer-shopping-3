var loadingBoxPuffDuration = 1.7;
var pageBoxBlindDownDuration = 4.0;
var ajaxLoadingImages = {'loading': "/public/images/loading.gif",'success': "/public/images/success.png",'failed': "/public/images/failed.gif"};

function go ( url, options )
{	
	if ( typeof options != 'undefined' )
		 new Ajax.Updater( wrapperElement, url, options);
	else new Ajax.Updater( wrapperElement, url, {asynchronous:true, evalScripts:true});
}
function goProducts ( url, options )
{	
		if ( typeof options != 'undefined' )
			 new Ajax.Updater( 'page_content', url, options);
		else new Ajax.Updater( 'page_content', url, {method: 'GET', asynchronous:true, evalScripts:true});
		
		if ( typeof options != 'undefined' )
			 new Ajax.Updater( 'col-one-content', '/ajax/get-products-menu', options);
		else new Ajax.Updater( 'col-one-content', '/ajax/get-products-menu', {asynchronous:true, evalScripts:true});
		
}

var blockLoadingIcon = false;
function ComputerShopping3()
{
	this.ajaxActiveX       = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP"];
	this.isAjaxLoading     = false;
	this.wrapperElement    = '';

    this.start = function ( ) 
    {
    };
    
    /*****************************/
    /* AJAX STATUS LOADING LAYER */
    /*****************************/
    
    this.loadPage = function ( div, url, options )
    {
	    wrapperElement = div;
	    
		if ( this.isSet( div ) )
		{
	    	Effect.BlindUp( wrapperElement, { duration: 3.00, queue: { position: 'front', scope: 'pageLoading' } });
	    	Effect.BlindDown( "sep", { duration: 1.20, queue: { position: 'front', scope: "pageSeparator" } });
		}
		
		$('loading_box').hide();
		$('loading_box').setStyle({ fontSize: '13px', paddingBottom: '10px', border: '1px solid orange', color: 'orange', textAlign: 'center', opacity:1, width:'60px', height:'60px' });
		$('loading_box').innerHTML = '<img id="loading_pic" src="'+ajaxLoadingImages.loading+'" alt="Loading..." /><br /><br />Loading';
		$('loading_box').appear({duration: 2.00,queue: {position: 'end', scope:"pageSeparator" }});
		
		var c = $('menu').select('li');
		
		if ( this.isSet( c ) )
		{
			c.each(function(e,i){ if ( e.hasClassName( 'active' ) ) e.removeClassName('active'); });
			c.each(function(e,i){ var el = e.childElements(); if ( typeof el[0].href != 'undefined' && el[0].pathname == url ) e.toggleClassName('active');});
		}
		
		if ( this.isSet( options ) )
    		 setTimeout( 'go("' + url + '",' + options + ');', 4000 ); // new Ajax.Updater( wrapperElement, url, options);
    	else setTimeout( 'go("' + url + '");', 4000 );                 // new Ajax.Updater( wrapperElement, url,{asynchronous:true, evalScripts:true});
    		
    	return false;
    };
    
    this.loadProductsPage = function ( url, options )
    {
    	blockLoadingIcon = true;
    	$('productPageBlocker').innerHTML = '<p style="text-align:center;"><img id="loading_pic" src="'+ajaxLoadingImages.loading+'" alt="Loading..." /><br /><br /><h2 class="title">Attendere prego...</h2> <br /><br />Stiamo caricando i prodotti che hai selezionato!<p>';
    	
		Effect.BlindDown( 'productPageBlocker', { duration: 3.00, queue: { position: 'start', scope: 'pageLoading3' } });
		Effect.Fade( 'page_content', { duration: 3.00, queue: { position: 'start', scope: 'pageLoading' } });
		Effect.Fade( 'col-one-content', { duration: 3.00, queue: { position: 'start', scope: 'pageLoading2' } });
				
		if ( this.isSet( options ) )
    		 setTimeout( 'goProducts("' + url + '",' + options + ');', 4000 ); // new Ajax.Updater( wrapperElement, url, options);
    	else setTimeout( 'goProducts("' + url + '");', 4000 );                 // new Ajax.Updater( wrapperElement, url,{asynchronous:true, evalScripts:true});
    		
    	return false;
    };

	this.switchLoadingStatus = function ( )
	{
		this.isAjaxLoading = ( this.isAjaxLoading ) ? false : true;
		
		if ( ! blockLoadingIcon )
		{
			if ( this.isAjaxLoading ) 
			{	
				$('loading_box').setStyle({ fontSize: '13px', paddingBottom: '10px', border: '1px solid orange', color: 'orange', textAlign: 'center', display: 'block', opacity:1, width:'60px', height:'60px' });
				$('loading_box').innerHTML = '<img id="loading_pic" src="'+ajaxLoadingImages.loading+'" alt="Loading..." /><br /><br />Loading';
			} else {
				$('loading_box').setStyle({ fontSize: '13px', paddingBottom: '10px',border: '1px solid green', color: 'green', textAlign: 'center', display: 'block', opacity:1, width:'60px', height:'60px' });
				$('loading_box').innerHTML = '<img id="loading_pic" src="'+ajaxLoadingImages.success+'" alt="Success!!!" /><br /><br />Success!';
				
		    	Effect.BlindUp("sep", { duration: 1.20, queue: { position: "end", scope: "pageSeparator" } });
				$('loading_box').puff( {duration:loadingBoxPuffDuration, queue: { position: 'end', scope: 'loading' }} );
				Effect.BlindDown( wrapperElement, { duration: pageBoxBlindDownDuration, queue: { position: 'end', scope: 'pageLoading' }});
			}
		} else {
			
			if ( this.isAjaxLoading ) 
			{
			} else {
				Effect.BlindUp( 'productPageBlocker', { duration: 3.00, queue: { position: 'end', scope: 'pageLoading3' } });
				Effect.Appear( 'page_content', { duration: 3.00, queue: { position: 'end', scope: 'pageLoading' } });
				Effect.Appear( 'col-one-content', { duration: 3.00, queue: { position: 'end', scope: 'pageLoading2' } });
				blockLoadingIcon = false;
			}
		}
	};

	this.switchFailedLoadingStatus = function ( )
	{
		$('loading_box').setStyle({ fontSize: '13px', paddingBottom: '10px',border: '1px solid red', color: 'red', textAlign: 'center', display: 'block', opacity:1, width:'260px', height:'60px' });
		$('loading_box').innerHTML = '<img src="'+ajaxLoadingImages.failed+'" alt="Failed!!!" /><br /><br />Failed!<br />Check your connection or try again later.';
		
		if ( this.isAjaxLoading )
			$('loading_box').fade( {duration:5.7} );
	};
    
    /***************/
    /* DUMP METHOD */
    /***************/
    
    this.dumpElement = function ( data, level ) 
    {
		var dumped_text = '';
		if( ! level ) level = 0;
	
		var level_padding = '';
		for( var j = 0; j < level + 1; j++ ) level_padding += '    ';
		
		if( typeof( arr ) == 'object' )
			for( var item in data ) {
				var value = data[item];
				
				if(typeof(value) == 'object') {
					dumped_text += level_padding + '\'' + item + '\' ...\n';
					dumped_text += dump( value, level + 1 );
				} else dumped_text += level_padding + '\'' + item + '\' => "' + value + '"\n';
			}
		else dumped_text = '===>' + data + '<===(' + typeof( data ) + ')';

		return dumped_text;
	};
    
    /******************/
    /* OTHERS METHODS */
    /******************/
    
    this.sanitizeData = function ( string )  
    { 
    	return new String(string).replace('/[\n\r\t]/g','').replace('/[ +\n\r\t]/g',' '); 
    };

	this.getURLVarsByArray = function ( array )
	{
		var ret;
		for ( var key in array )
			ret += '&' + key + '=' + encodeURIComponent( array[key] );
			
		return ret.substr( 1 );
	};
	
    this.redirect = function ( url ) 
    { 
    	document.location.href = url;
    };
	
    this.isSet = function ( data ) 
    { 
    	return ( typeof data != 'undefined' &&
    	         data != null ) ? true : false;
    };
}
var wrapperElement;
var ajaxHandler;

var CS3 = new ComputerShopping3();
CS3.start;

document.observe("dom:loaded", function() {
	Ajax.Responders.register({
		onCreate : CS3.switchLoadingStatus,
		onComplete : CS3.switchLoadingStatus,
		onFailure : CS3.switchFailedLoadingStatus,
		onException : CS3.switchFailedLoadingStatus
	});

	$('loading_box').puff( {duration:loadingBoxPuffDuration} );
	Effect.BlindUp('sep', {duration: 1.20, queue: { position: 'end', scope: 'separator' }} );
	Effect.BlindDown( 'page_box', { duration: pageBoxBlindDownDuration});
});



