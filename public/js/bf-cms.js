var BFCMS = {
	start:function(){
	},
	rand: function ( min, max ) {
    // http://kevin.vanzonneveld.net
    // +   original by: Leslie Hoare
    // +   bugfixed by: Onno Marsman
    // *     example 1: rand(1, 1);
    // *     returns 1: 1
    var argc = arguments.length;
    if (argc == 0) {
        min = 0;
        max = 2147483647;
    } else if (argc == 1) {
        throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
},
	switchImage:function( element, image ){ 
		element.src = "/public/images/" + image; 
	},
	
	switchBgImage:function( element, image ){ 
		element.style.backgroundImage = "url( \"/public"+ image+"\" )"; 
	},
	
	switchButtonImage:function( element, image, hover )
	{ 
	if ( hover ) element.style.backgroundImage = "url( \'/public/images/index." + image + ".icon.hover.jpg\' )"; 
	else element.style.backgroundImage = "url( \'/public/images/index." + image + ".icon.jpg\' )"; 
	},
	
	dump:function( what, level )
	{
		var dumped_text = "";
		if(!level) level = 0;
	
		//The padding given at the beginning of the line.
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";
	
		if(typeof(arr) == 'object') { //Array/Hashes/Objects
			for(var item in what) {
				var value = what[item];
	
				if(typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Stings/Chars/Numbers etc.
			dumped_text = "===>"+what+"<===("+typeof(what)+")";
		}
		return dumped_text;
	},
	
	getVar:function( what ) {
         get_string = document.location.search;         
         return_value = '';
         
         do { //This loop is made to catch all instances of any get variable.
            name_index = get_string.indexOf(name + '=');
            
            if(name_index != -1)
              {
              get_string = get_string.substr(name_index + name.length + 1, get_string.length - name_index);
              
              end_of_value = get_string.indexOf('&');
              if(end_of_value != -1)                
                value = get_string.substr(0, end_of_value);                
              else                
                value = get_string;                
                
              if(return_value == '' || value == '')
                 return_value += value;
              else
                 return_value += ', ' + value;
              }
            } while(name_index != -1)
            
         //Restores all the blank spaces.
         space = return_value.indexOf('+');
         while(space != -1)
              { 
              return_value = return_value.substr(0, space) + ' ' + 
              return_value.substr(space + 1, return_value.length);
							 
              space = return_value.indexOf('+');
              }
          
         return(return_value);        
	},
	
	resetForm:function( formId, action )
	{
		if ( action != '' ) document.getElementById(formId).action = action;
		else document.getElementById(formId).action = '#';
	}
}

BFCMS.start;