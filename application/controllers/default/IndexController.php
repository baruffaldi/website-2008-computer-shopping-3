<?php
/**
 * MV-Controller: Index
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class IndexController extends Zend_Controller_Action 
{
    public function indexAction( ) { }
    
    public function loginNeededAction( ) { }
    
    public function feedsAction( ) { }
    
    public function sitemapAction( ) { }
    
    public function underConstructionAction( ) { }
    
    public function subscribeNewsletterAction( ) { }
    
    public function sendMailAction( ) { }
}