<?php
/**
 * MV-Controller: Feeds
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

class AdministrationController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'index', 'index' ); }
    
    public function galleryAction( ) { }
    
    public function productsAction( ) { }
    
    public function pagesAction( ) { }
}