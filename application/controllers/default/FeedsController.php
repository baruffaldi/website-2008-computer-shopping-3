<?php
/**
 * MV-Controller: Feeds
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

class FeedsController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'feeds', 'index' ); }
    
    public function sitemapXmlAction( ) { }
    
    public function sitemapXslAction( ) { }
    
    public function productsXmlAction( ) { }
    
    public function productsUpdatesXmlAction( ) { }
    
    public function galleryXmlAction( ) { }
    
    public function newslettersXmlAction( ) { }
}