<?php
/**
 * MV-Controller: AJAX Back-End
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

class AjaxController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'index', 'index' ); }
    
    public function redirAction( ) { header( 'Location: ' . base64_decode( $_GET['u'] ) ); }
    
    public function underConstructionAction( ) { }
    
    public function urlDefinitionAction( ) { }
    
    public function iePngFixAction( ) { }
    
    public function getCarouselDataAction( ) { }
    
    public function getRandomImageAction( ) { }
    
    public function getProductsMenuAction( ) { }
}