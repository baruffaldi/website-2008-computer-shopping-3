<?php
/**
 * MV-Controller: Browse Front-End
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */
class BrowseController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'index', 'index' ); }
    
    public function productsAction( ) { }
    
    public function productAction( ) { }
    
    public function galleryAction( ) { }
}