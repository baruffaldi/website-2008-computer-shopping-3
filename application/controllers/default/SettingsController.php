<?php
/**
 * MV-Controller: AJAX Back-End
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

class SettingsController extends Zend_Controller_Action 
{
    public function indexAction( ) { $this->_helper->redirector( 'index' ); }
    
    public function setAction( ) 
    {
    	$params = $this->_getAllParams();

    	foreach ( $params as $field => $value )
	    	switch( $field )
	    	{
	    		default:
	    			break;
	    			
	    		case 'language':
					$_SESSION['language'] = $value;
	    			break;
	    			
	    		case 'theme':
					$_SESSION['theme'] = $value;
	    			break;
	    	}

	    if ( empty( $_SERVER['HTTP_REFERER'] ) ) $_SERVER['HTTP_REFERER'] = '/';
		header( 'Location: '. $_SERVER['HTTP_REFERER'] );
    }
}