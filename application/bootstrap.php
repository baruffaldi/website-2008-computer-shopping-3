<?php
/**
 * Application bootstrap
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage ShortInternetLink
 */

// *** Default Settings
     $_SITE['config']['env']['language'] = array( 'full' => 'en-US', 'short' => 'en' );
     $_SITE['config']['env']['theme'] = 'default';

// *** CMS Settings 
     define( '__VERSION__', '1.3' );
     define( '__GENERATOR__', "BF CMS " . __VERSION__ );

// *** Environment Settings 
     $_SITE['config']['fs']['path_documentroot']  = dirname( __FILE__ )                         . DIRECTORY_SEPARATOR . '..';
     $_SITE['config']['fs']['path_public']        = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'public';
     $_SITE['config']['fs']['path_lib']           = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'library';
     $_SITE['config']['fs']['path_app']           = $_SITE['config']['fs']['path_documentroot'] . DIRECTORY_SEPARATOR . 'application';
     $_SITE['config']['fs']['path_bootstrap']     = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'bootstrap';
     $_SITE['config']['fs']['path_templates']     = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'templates';
     $_SITE['config']['fs']['path_config']        = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'config';
     $_SITE['config']['fs']['path_log']           = $_SITE['config']['fs']['path_app']          . DIRECTORY_SEPARATOR . 'logs';
     
// *** PHP Settings 
     $_SITE['config']['env']['type']  = ( isset( $_SERVER['ENV'] ) ) ? $_SERVER['ENV'] : 'development';
     $_SITE['config']['env']['debug'] = ( isset( $_GET['debug'] ) 
                                       || isset( $_POST['debug'] ) 
                                       || $_SERVER['ENV'] = 'development' ) ? FALSE : FALSE;
                                       
     set_include_path( get_include_path( ) . PATH_SEPARATOR 
                     . $_SITE['config']['fs']['path_lib'] );
     
     if ( $_SITE['config']['env']['debug'] ) error_reporting( E_ALL ^ E_NOTICE );
     if ( $_SITE['config']['env']['debug'] ) ini_set( 'display_errors', 1 );
     else ini_set( 'display_errors', 0 );

// *** Authentication Handler
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'auth.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'auth.php';

// *** Configuration Handler
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'config.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'config.php';

// *** Themes
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'themes.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'themes.php';

// *** Languages
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'languages.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'languages.php';

// *** Router
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'router.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'router.php';

// *** MVC Handlers
     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'loader.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'loader.php';

     if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'layout.php' ) )
     require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . 'layout.php';

// *** Include all Classes
     foreach ( explode( ',', $_SITE['config']['handler']->init->classes ) as $class )
         if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Classes' 
                                                              . DIRECTORY_SEPARATOR . trim( $class ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Classes' 
                                                              . DIRECTORY_SEPARATOR . trim( $class ) . '.php';
     
     unset( $class );

// *** Include urlTube Classes
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Exception.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Exception.php';
                                                     
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Interface.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Interface.php';
                                                     
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Abstract.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Abstract.php';
                                                     
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Abstract_Import.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Abstract_Import.php';
                                                     
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Abstract_Manage.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Abstract_Manage.php';
                                                     
     if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                          . DIRECTORY_SEPARATOR . 'Class.php' ) )
     require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                     . DIRECTORY_SEPARATOR . 'Class.php';
     
     if ( isset( $_SITE['config']['handler']->sources ) )
     foreach ( explode( ',', $_SITE['config']['handler']->sources ) as $urlTube )
         if ( file_exists( $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                              . DIRECTORY_SEPARATOR . 'Drivers' 
                                                              . DIRECTORY_SEPARATOR . trim( $urlTube ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_lib'] . DIRECTORY_SEPARATOR . 'Seller' 
                                                              . DIRECTORY_SEPARATOR . 'Drivers' 
                                                              . DIRECTORY_SEPARATOR . trim( $urlTube ) . '.php';
     
     unset( $urlTube );

// *** Include all init Modules
     foreach ( explode( ',', $_SITE['config']['handler']->init->modules ) as $initModule )
         if ( file_exists( $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . trim( $initModule ) . '.php' ) ) 
              require_once $_SITE['config']['fs']['path_bootstrap'] . DIRECTORY_SEPARATOR . trim( $initModule ) . '.php';
     
     unset( $initModule );
     
	 if ( ! is_object( $_SITE['Seller'] ) )
	 $_SITE['Seller'] = new Seller( $_SITE['database']['handler'] );