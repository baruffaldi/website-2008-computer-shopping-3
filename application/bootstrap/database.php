<?php
/**
 * Bootstrap: Database
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Table' . DIRECTORY_SEPARATOR . 'Abstract.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Profiler' . DIRECTORY_SEPARATOR . 'Firebug.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Adapter' . DIRECTORY_SEPARATOR . 'Exception.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Db' . DIRECTORY_SEPARATOR . 'Adapter' . DIRECTORY_SEPARATOR . 'Pdo' . DIRECTORY_SEPARATOR . 'Mysql.php';

$_SITE['database']['handler']  = Zend_Db::factory( $_SITE['config']['handler']->database );
$_SITE['database']['tables']   = $_SITE['database']['handler']->listTables( );
$_SITE['database']['isActive'] = isset( $_SITE['database']['tables'][0] );

$_SITE['database']['profiler']['handler'] = new Zend_Db_Profiler_Firebug('All DB Queries');
$_SITE['database']['profiler']['handler']->setEnabled( TRUE );

$_SITE['database']['handler']->setProfiler( $_SITE['database']['profiler']['handler'] );
$_SITE['database']['handler']->setFetchMode( Zend_Db::FETCH_ASSOC );

Zend_Db_Table_Abstract::setDefaultAdapter( $_SITE['database']['handler'] );

