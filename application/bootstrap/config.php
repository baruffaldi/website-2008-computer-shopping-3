<?php
/**
 * Bootstrap: Configuration Handlers
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . 'Config.php';
require_once 'Zend' . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR . 'Ini.php';

foreach ( glob( $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                'config' . DIRECTORY_SEPARATOR . '*.ini' ) as $configFile )
{
     $patterns[0] = '/'.str_replace( '/', '\\/', $_SITE['config']['fs']['path_app'] . DIRECTORY_SEPARATOR . 
                                                                              'config' . DIRECTORY_SEPARATOR ).'/';
     $patterns[1] = '/.ini/';
     $replacements[0] = NULL;
     $replacements[1] = NULL;
     $configName = preg_replace( $patterns, $replacements, $configFile );
     $_SITE[$configName]['handler'] = new Zend_Config_Ini( $configFile, $_SITE['config']['env']['type'], TRUE );
}

if ( $_SITE['config']['env']['type'] == 'development' ) 
    $_SITE['config']['handler']->redirect->domain = "test.{$_SITE['config']['handler']->redirect->domain}";