<?php
/**
 * Bootstrap: Multi-Theme
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

if ( isset( $_SESSION['theme'] ) ) {
     $_SITE['config']['env']['theme'] = $_SESSION['theme'];
// *** default themes
} elseif ( ! empty( $_SITE['config']['handler']->site->theme ) ) {
     $_SITE['config']['env']['theme'] = $_SITE['config']['handler']->site->theme;
} else {
     $_SITE['config']['env']['theme'] = 'default';
}