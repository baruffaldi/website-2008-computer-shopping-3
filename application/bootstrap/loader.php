<?php
/**
 * Bootstrap: Front-End Loader Helpers
 *
 * @author Filippo Baruffaldi <filippo@baruffaldi.info>
 * @package BZCms
 * @subpackage UrlTUBE
 */

require_once 'Zend' . DIRECTORY_SEPARATOR . '/Loader.php';

Zend_Loader::registerAutoload( );

$_SITE['frontController']['handler'] = Zend_Controller_Front::getInstance();

$_SITE['frontController']['handler']->setControllerDirectory( $_SITE['config']['fs']['path_controller'] );

$_SITE['frontController']['handler']->setParam( 'env', $_SITE['config']['env']['type'] );