<?php

interface Seller_Import_Interface
{
	public function updateSource( $source = NULL );
	public function updateProduct( $id = 0 );
	public function updateProducts( );
	
	public function getArrayFeedByXML( $xml, $string = FALSE );
	public function getArrayFeedByCSV( $csv, $string = FALSE );
	
	public static function getDataBaseSourceFields( );
	public static function getDataBaseProductFields( );
	
	public static function getXmlString( );
	public static function getCsvString( );
	
	public static function getXmlArray( );
	public static function getCsvArray( );
	
	public function removeAccents( $string );
	
	public static function xml2array( $contents, $get_attributes=1, $priority = 'tag' );
}

interface Seller_Interface
{	
	public function removeAccents( $string );
	
	public static function xml2array( $contents, $get_attributes=1, $priority = 'tag' );
}