<?php

class Seller extends Seller_Abstract
{
	public $filters = array( );
	
	public $query = NULL;
	public $queryId = NULL;
	
	public $categories = array( );
	public $subCategories = array( );
	
	public $manufacturers = array( );

	public $categoriesStructure = array( );
	
	public function __construct( $db )
	{
		parent::__construct( $db );

		global $_SITE;
		
		if ( ! isset( $_GET['resetFilters'] ) )
			 $this->filters = $_SESSION['filters'];
		
		if ( isset( $_GET['updateFilters'] ) && $_GET['updateFilters'] )
		{
			foreach( $_GET['filters'] as $filter => $value )
				$this->filters[$filter] = $value;

			if ( isset( $_GET['filters']['s'] ) )
			$this->filters['sd'] = ( $this->filters['sd'] == 'ASC' ) ? 'DESC' : 'ASC';

			$_SESSION['filters'] = $this->filters;
		}
		
		$this->query = ( ! empty( $_GET['q'] ) ) ? $_GET['q'] : NULL;
		$this->queryId = ( ! empty( $_GET['ui'] ) ) ? $_GET['ui'] : NULL;
			
		$this->getCategories( );
		$this->getManufacturers( );
	}

	public function getLastProductsUpdates( )
	{
		if ( ! is_null( $this->queryId ) )
			switch ( $this->query )
			{
				default:
					$columns = array( 'added_sources_ids', 'added_products_ids', 'changed_prices_ids', 'changed_products_ids' );
					break;
					
				case 'as':
					$columns = array( 'added_sources_ids' );
					break;
					
				case 'ap':
					$columns = array( 'added_products_ids' );
					break;
					
				case 'cpr':
					$columns = array( 'changed_prices_ids' );
					break;
					
				case 'cp':
					$columns = array( 'changed_products_ids' );
					break;
			}
		else $columns = array( 'id', 'update', 'added_sources' => 'SUM(added_sources)', 'added_products' => 'SUM(added_products)', 'changed_products' => 'SUM(changed_products)', 'changed_prices' => 'SUM(changed_prices)' );

		$news = $this->dbHandler->select( )
		                        ->from( $this->updatesTable, $columns )
		                        ->limit( 4 )
		                        ->order( 'id DESC' )
		                        ->group( 'update' );
		                        
		if ( ! is_null( $this->queryId ) )
			$news->where( 'id = ?', $this->queryId );
		                                     
		$news = $news->query( );
		
		return $news->fetchAll(); 
	}
	
	public function getProducts( $filters = NULL )
	{
		global $_SITE;
		
		if ( ! is_null( $this->queryId ) )
		{
			$news = $this->getLastProductsUpdates( );
			$ids = array( );
			foreach ( $news[0] as $vet )
				if ( ! is_null( $vet ) )
					foreach( unserialize( $vet ) as $id )
						if ( ! in_array( $id, $ids ) )
							$ids[] = $id;
		}
		
		$q = $this->dbHandler->select( )
		                          ->from( $this->productsTable, array( 'id_product', 'update', 'cod', 'category', 'sub_category', 'manufacturer', 'description' => 'LOWER(description)', 'bonus_percent', 'base_availability', 'base_unavailability', 'affils_availability', 'affils_unavailability', 'price', 'price_full' => '( price_full + ( price_full * '.$_SITE['config']['handler']->seller->cashonhand.'/100 ) )' ) )
			                      ->where( 'affils_availability >= 1 OR base_availability >= 1' );

		$this->processFilters( $q );
		
		if ( is_array( $filters ) )
		{
			
			if ( ! empty( $filters['i'] ) )
			     $q->where( 'id_product = ?', $filters['i'] );
			
			if ( ! empty( $filters['m'] ) )
			     $q->where( 'manufacturer = ?', $filters['m'] );
			
			if ( ! empty( $filters['c'] ) )
			     $q->where( 'category = ?', $filters['c'] );
			     
			if ( ! empty( $filters['sc'] ) )
			     $q->where( 'sub_category = ?', $filters['sc'] );
			     
			if ( ! empty( $filters['s'] ) )
			     $q->order( array( "{$filters['s']} {$filters['sd']}" ) );
			     
			if ( ! empty( $filters['l'] ) )
			     $q->limit( $filters['l'], $filters['ls'] );
			     
			if ( ! empty( $filters['p'] ) )
			     $q->where( '( price_full + ( price_full * 15 / 100 ) ) <= ?', $filters['p'] );
		}
		    
		if ( is_array( $ids ) )
		{
			$q->order( array( 'id_product DESC' ) );
			foreach ( $ids as $id )
				$w .= " OR id_product = $id";
				
			$q->where( substr( $w, 4 ) );
		}
		            
		$h = $q->query();
		return $h->fetchAll( );
	}
	
	public function processFilters( $queryHandler )
	{
		if ( ! empty( $this->filters['m'] ) )
		     $queryHandler->where( 'manufacturer = ?', $this->filters['m'] );
		
		if ( ! empty( $this->filters['i'] ) ) 
		     $queryHandler->where( 'id_product = ?', $this->filters['i'] );
		
		if ( ! empty( $this->filters['c'] ) )
		     $queryHandler->where( 'category = ?', $this->filters['c'] );
		     
		if ( ! empty( $this->filters['sc'] ) )
		     $queryHandler->where( 'sub_category = ?', $this->filters['sc'] );
		     
		if ( ! empty( $this->filters['s'] ) )
		     $queryHandler->order( array( "{$this->filters['s']} {$this->filters['sd']}" ) );
		     
		if ( ! empty( $this->filters['l'] ) ) 
		     $queryHandler->limit( $this->filters['l'], $this->filters['ls'] );
		     
		if ( ! empty( $this->filters['p'] ) ) 
		     $queryHandler->where( '( price_full + ( price_full * 15 / 100 ) ) <= ?', $this->filters['p'] );
		     
		return $queryHandler;
	}
	
	public function getManufacturers( )
	{
		if ( empty( $this->manufacturers ) )
		{
			$q = $this->dbHandler->select( )
			                     ->distinct( )
			                     ->from( $this->productsTable, array( 'manufacturer' ) )
			                     ->where( 'affils_availability >= 1 OR base_availability >= 1' )
			                     ->order( array( 'manufacturer ASC' ) );
			
			$this->processFilters( $q );
			
			$h = $q->query();
			$c = $h->fetchAll( );
			
			foreach ( $c as $manufacturer )
			    $this->manufacturers[] = $manufacturer['manufacturer'];
		}
	}
	
	public function getCategories( )
	{
		if ( empty( $this->categories ) )
		{
			$q = $this->dbHandler->select( )
			                     ->distinct( )
			                     ->from( $this->productsTable, array( 'category' ) )
			                     ->where( 'affils_availability >= 1 OR base_availability >= 1' )
			                     ->order( array( 'category ASC', 'sub_category ASC' ) );
			
			if ( ! empty( $this->filters['c'] ) )
			     $q->where( 'category = ?', $this->filters['c'] );
			     
			$h = $q->query();
			$c = $h->fetchAll( );
			
			foreach ( $c as $product )
			    $this->categories[] = $product['category'];
		}
		
		if ( empty( $this->subCategories ) )
		{
			$q = $this->dbHandler->select( )
			                     ->distinct( )
			                     ->from( $this->productsTable, array( 'category', 'sub_category' ) )
			                     ->where( 'affils_availability >= 1 OR base_availability >= 1' );
			
			$this->processFilters( $q );
			
			/*if ( ! empty( $this->filters['c'] ) )
			     $q->where( 'category = ?', $this->filters['c'] );
			     
			if ( ! empty( $this->filters['sc'] ) )
			     $q->where( 'sub_category = ?', $this->filters['sc'] );*/
			                
			$h = $q->query();
			$c = $h->fetchAll( );
			
			foreach ( $c as $product )
				$this->subCategories[] = $product['sub_category'];
			
			foreach ( $c as $product )
				$this->categoriesStructure[$product['category']][] = $product['sub_category'];
		}
	}
}