<?php


abstract class Seller_Abstract_Import extends Seller_Abstract
{
	protected $sourceId = 0;
	
	protected $xmlString = NULL;
	protected $csvString = NULL;
	
	protected $xmlArray = array( );
	protected $csvArray = array( );
	                                   
	public $sourceArray = array( );
	public $productsArray = array( );
	
	protected $sourceFieldsMapping = array( );
	protected $productFieldsMapping = array( );
	                                   
	public $finalSource = array( );
	public $finalProducts = array( );
	
	public $processedProducts = array( 'added_sources' => 0, 'added_products' => 0, 'changed_products' => 0, 'changed_prices' => 0 );
	public $processedProductsIds = array( );
	
	
	public function __construct( $db, $data, $type = 'xml', $string = FALSE )
	{
		parent::__construct( $db );
		
		if ( ! empty( $data ) )
		switch( strtolower( $type ) )
		{
			default:
				throw new Exception( "Invalid import source type specified! [$type]", 1 );
				break;
			
			case 'xml':
				$this->xmlArray = $this->getArrayFeedByXML( $data, $string );
				break;
			
			case 'csv':
				$this->csvArray = $this->getArrayFeedByCSV( $data, $string );
				break;
		}
	}
	
	public function updateTemplate( )
	{
		$this->updateSource( $this->sourceArray );
		$this->updateProducts( $this->productsArray );
	}
	
	public function updateSource( $source = NULL )
	{
		if ( ! is_null( $source ) )
		foreach( $this->dbSourceFields as $key )
			if ( ! is_null( $source[$this->sourceFieldsMapping[$key]] ) )
				if ( is_array( $source[$this->sourceFieldsMapping[$key]] ) ) $this->finalSource[$key] = implode( ',', $source[$this->sourceFieldsMapping[$key]] );
				else $this->finalSource[$key] = $source[$this->sourceFieldsMapping[$key]];
	}
	
	public function updateProduct( $id = 0 )
	{
		foreach( $this->dbProductFields as $key )
			if ( ! is_null( $this->productsArray[$id][$this->productFieldsMapping[$key]] ) )
				if ( is_array( $this->productsArray[$id][$this->productFieldsMapping[$key]] ) ) $this->finalProducts[$id][$key] = implode( ',', $this->productsArray[$id][$this->productFieldsMapping[$key]] );
				else $this->finalProducts[$id][$key] = $this->productsArray[$id][$this->productFieldsMapping[$key]];
	}
	
	public function updateProducts( $products = NULL )
	{
		if ( ! is_null( $products ) )
		{
			$productsId = array_keys( $products );
	
			if ( count( $productsId ) >= 2 )
				foreach( $productsId as $id )
					if ( is_int( $id ) ) $this->updateProduct( $id );
		}
	}
	
	public function getArrayFeedByXML( $xml, $string = FALSE )
	{
		if ( ! $string ) {
			$this->xmlString = file_get_contents( $xml );
		} else $this->xmlString = $xml;
		
		return $this->xml2array( str_replace( array( '�' ), array( '(c)' ), $this->removeAccents( $this->xmlString ) ) );
	}
	
	public function getArrayFeedByCSV( $csv, $string = FALSE )
	{
		if ( ! $string ) {
			$fs = fopen( $csv, 'r' );
			$this->csvString = fgets( $fs, filesize( $csv ) );
		}
		
		return fgetcsv( $fs );
	}
	
	public static function getDataBaseSourceFields( ) { return $this->db_source_fields; }
	public static function getDataBaseProductFields( ) { return $this->db_product_fields; }
	
	public static function getXmlString( ) { return $this->xmlString; }
	public static function getCsvString( ) { return $this->csvString; }
	
	public static function getXmlArray( ) { return $this->xmlArray; }
	public static function getCsvArray( ) { return $this->csvArray; }
    
    public function processSource( )
    {
    	$q = $this->dbHandler->select( )
    	                     ->from( $this->sourcesTable, array( 'id_source' ) )
    	                     ->where( 'name = ?', $this->finalSource['name'] )
    	                     ->limit( 1 );
    	
		$h = $q->query( );
		$col = $h->fetchAll( );
		
    	if ( empty( $col[0]['id_source'] ) ) {
    		$this->dbHandler->insert( $this->sourcesTable, $this->finalSource );
    		$this->sourceId = $this->dbHandler->lastInsertId();
    		$this->processedProducts['added_sources']++;
    	} else {
    		$this->dbHandler->update( $this->sourcesTable, $this->finalSource, "id_source = {$col[0]['id_source']}" );
    		$this->sourceId = $col[0]['id_source'];
    	}
    	
    	$this->processedProductsIds['added_sources'][] = $this->sourceId;
    }
    
    public function processProduct( $product )
    {
    	$q = $this->dbHandler->select( )
    	                     ->from( $this->productsTable, array( 'id_product', 'price' ) )
    	                     ->where( 'cod = ?', $product['cod'] )
    	                     ->where( 'id_source = ?', $this->sourceId )
    	                     ->limit( 1 );
    	
		$h = $q->query( );
		$col = $h->fetchAll( );
		
		if ( ! is_null( $col[0]['price'] ) && $col[0]['price'] != $product['price'] ) {
			$this->processedProducts['changed_prices']++;
    		$this->processedProductsIds['changed_prices'][] = $col[0]['id_product'];
		}
		
    	if ( empty( $col[0]['id_product'] ) ) {
    		$this->dbHandler->insert( $this->productsTable, array_merge( array( 'id_source' => $this->sourceId ), $product ) );
    		$this->processedProducts['added_products']++;
    		$this->processedProductsIds['added_products'][] = $this->dbHandler->lastInsertId();
    	} else {
    		$this->dbHandler->update( $this->productsTable, array_merge( array( 'id_source' => $this->sourceId ), $product ), "id_product = {$col[0]['id_product']}" );
    		$this->processedProducts['changed_products']++;
    		$this->processedProductsIds['changed_products'][] = $col[0]['id_product'];
    	}
    }
    
    public function processProducts( )
    {
    	foreach( $this->finalProducts as $product )
    		$this->processProduct( $product );
    	
    	if ( $this->processedProducts['added_sources'] >= 1 )
    		$this->processedProducts['added_sources_ids'] = serialize( $this->processedProductsIds['added_sources'] );
    	if ( $this->processedProducts['added_products'] >= 1 )
    		$this->processedProducts['added_products_ids'] = serialize( $this->processedProductsIds['added_products'] );
    	if ( $this->processedProducts['changed_prices'] >= 1 )
    		$this->processedProducts['changed_prices_ids'] = serialize( $this->processedProductsIds['changed_prices'] );
    	if ( $this->processedProducts['changed_products'] >= 1 )
    		$this->processedProducts['changed_products_ids'] = serialize( $this->processedProductsIds['changed_products'] );
    	_dump($this->processedProducts);
		$this->dbHandler->insert( $this->updatesTable, $this->processedProducts );
    }
}