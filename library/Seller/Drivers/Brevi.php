<?php

class Seller_Brevi extends Seller_Abstract_Import
{
	 protected $sourceFieldsMapping = array( 'name' => 'ragione',
		             	                     'cap' => 'cap',
		                	                 'street' => 'via',
		                    	             'civic-address' => 'civico',
		                        	         'city' => 'citta',
		                            	     'province' => 'provincia',
		                                	 'phone' => 'telefono',
			                                 'fax' => 'fax',
		    	                             'sites' => 'sito',
		        	                         'email' => 'email' );
	
	protected $productFieldsMapping = array( 'cod' => 'codice', 
		                                     'category' => 'categoria_merceologica_primaria', 
		                                     'sub_category' => 'categoria_merceologica_secondaria', 
		                                     'manufacturer' => 'produttore', 
		                                     'description' => 'descrizione', 
		                                     'price' => 'prezzo_netto', 
		                                     'price_full' => 'prezzo_listino', 
		                                     'bonus_percent' => 'sconto', 
		                                     'affils_availability' => 'disponibilita_filiale', 
		                                     'affils_unavailability' => 'impegnato_filiale', 
		                                     'base_availability' => 'disponibilita_sede', 
		                                     'base_unavailability' => 'impegnato_sede' );
                                      
    public function __construct( $db, $data, $type = 'xml', $string = FALSE )
    {
    	parent::__construct( $db, $data, $type, $string );
    	
    	$vetName = $type . "Array";
    	
    	$this->sourceArray = $this->{$vetName}['listino']['testa']['anagrafica'];
    	$this->productsArray = $this->{$vetName}['listino']['articoli']['articolo'];

    	$this->updateTemplate(  );
    	
    	$this->processSource(  );
    	$this->processProducts(  );
    }
}