<?php

class XFeeds
{
    public $mimetype     = NULL;
    public $mime         = NULL;
    public $type         = NULL;
    
    public $service      = NULL;
    public $protocol     = NULL;
    public $feed         = NULL;
    public $feed_type    = 'rss';
    public $feed_handler = NULL;
    
    public $title        = NULL;
    public $description  = NULL;
    
    
    public function XFeeds( $options = array( ) )
    {
    	
    	foreach ( $options as $option => $value )
    		$this->$option = $value;
    		
    	
    }
    
    public function getUrlTubes( )
    {
    	global $_SITE;
    	
    	switch( $this->type )
    	{
    		default:
    			$_GET['resetFilters'] = 1;
				$d = new Seller( $_SITE['database']['handler'] );
				
				return $d->getProducts( array( 's' => 'update', 'sd' => 'DESC' ) );
    			break;
    			
    		case 'gallery':
				$q = $_SITE['database']['handler']->select( )
				                                  ->from( 'gallery' )
				                                  ->where( 'status = ?', 'Y' )
				                                  ->order( 'id DESC' )
				                                  ->limit( 16 );
				$h = $q->query( );

				return $h->fetchAll( );
    			
    		case 'updates':
				$q = $_SITE['database']['handler']->select( )
				                                  ->from( 'updates' )
				                                  ->order( 'id DESC' )
				                                  ->limit( 16 );
				$h = $q->query( );

				return $h->fetchAll( );
    			
    		case 'newsletters':
				$q = $_SITE['database']['handler']->select( )
				                                  ->from( 'newsletters' )
				                                  ->order( 'id DESC' )
				                                  ->limit( 16 );
				$h = $q->query( );

				return $h->fetchAll( );
    			break;
    	}
    }
    
    public function getFeed( $type = NULL )
    {
    	if ( ! is_null( $type ) ) $this->feed_type = $type;
    	
    	$UrlTubes = $this->getFeedArrayByUrlTubes( $this->getUrlTubes( ), $this->service );
    	
    	switch( $this->feed_type )
    	{
    		default:
    		case 'rss':
				$this->feed_handler = Zend_Feed::importArray( $UrlTubes, 'rss' );
    			break;
    			
    		case 'atom':
				$this->feed_handler = Zend_Feed::importArray( $UrlTubes, 'atom' );
    			break;
    	}
    	
    	$this->feed = $this->feed_handler->saveXML( );
    	
    	return $this->feed;
    }
    
    public function printFeed(  )
    {
    	if ( is_null( $this->feed_handler ) ) $this->getFeed( $this->feed_type );
    	
    	$this->feed_handler->send( );
    	
    	return $this->feed_handler;
    }
    
    public function getFeedInstance(  )
    {
    	if ( is_null( $this->feed_handler ) ) $this->getFeed( $this->feed_type );
    	
    	return $this->feed_handler;
    }
    
    public function getFeedArrayByUrlTubes( $urls = NULL, $service = NULL )
    {
    	global $_SITE;
    	
    	if ( is_null( $urls ) ) return FALSE;
    	
    	$feedArray = $this->getFeedArray( );

    	$feedArray['lastUpdate'] = $urls[0]['creation_date'];
        $feedArray['published']  = '454475222';
		$feedArray['itunes'] = array(
                     'author'       => trim( $_SITE['config']['handler']->site->admin ), // optional, default to the main author value
                     'image'        => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . '/public/themes/default/images/logo.png', // optional, default to the main image value
                     'subtitle'     => trim( $this->description ), // optional, default to the main description value
                     'summary'      => trim( $this->description ), // optional, default to the main description value
                     'explicit'     => 'clean', // optional
                     'category'     => array(
                                             array('main' => 'Computer Shopping 3', // required
                                                   'sub'  => 'Vendita materiale tecnologico' // optional
                                                   )
                                             // up to 3 rows
                                             ) // 'Category column and in iTunes Music Store Browse' // required*/
                     /*'owner'        => array(
                                             'name' => 'name of the owner', // optional, default to main author value
                                             'email' => 'email of the owner' // optional, default to main email value
                                             ), */// Owner of the podcast // optional
                     //'block'        => 'Prevent an episode from appearing (yes|no)', // optional
                     //'keywords'     => 'a comma separated list of 12 keywords maximum', // optional
                     //'new-feed-url' => 'used to inform iTunes of new feed URL location' // optional
                     );
                     
        $feedArray['entries'] = array( );

        if ( ! empty( $urls[0] ) )
        foreach( $urls as $id => $url )
        {
    	    if ( ! empty( $url ) )
    	    {
    	    	unset( $get );
    	    	switch( $this->type )
    	    	{
    	    		default:
    	    			$get = "i={$url['id_product']}";
    	    			$image = md5( $url['id_product'] ) . '.jpg';
    	    	
						$url['title'] = ( is_null( $url['title'] ) ) ? __LANG_NO_TITLE__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 25 ) );
						$url['description'] = ( is_null( $url['description'] ) ) ? __LANG_NO_DESCRIPTION__ : str_replace( "\n", '<br />', wordwrap( $url['description'], 35 ) );

						$dt = explode( ' ', $url['update'] );
						$d = explode( '-', $dt[0] );
						$t = explode( ':', $dt[1] );
				        $feedArray['entries'][$id]  = array('title'        => trim( strip_tags( "{$url['manufacturer']}: ". substr( $url['description'], 0, 32 ) ) ),
						                                   'link'         => "http://{$_SERVER['HTTP_HOST']}/browse/{$this->type}?$get",
						                                   'description'  => "<h2>" .trim( strip_tags( $url['description'] ) ) . "</h2> <br /> <br /><img src='http://{$_SERVER['HTTP_HOST']}/public/images/seller/thumbs/$image' alt='Thumbnail' />", // only text, no html, required
						                                   'lastUpdate'   => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'pubDate'      => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'source'       => array(
						                                                           'title' => trim( strip_tags( "{$url['manufacturer']}: ". substr( $url['description'], 0, 32 ) ) ), // required,
						                                                           'url' => "http://{$_SERVER['HTTP_HOST']}/browse/{$this->type}?$get" // required
						                                                           ), // original source of the feed entry // optional
						                                   'guid'         => trim( "http://{$_SERVER['HTTP_HOST']}/browse/{$this->type}?$get" ),
						                                   'content'      => trim( strip_tags( $url['description'] ) ) . '&nbsp;',
						                                   'enclosure'    => array( array( 'lenght' => 0, 'type' => 'image/jpeg', 'url' => "http://{$_SERVER['HTTP_HOST']}/public/images/seller/thumbs/$image" ) )/*,
						                                   //'comments'     => 'comments page of the feed entry', // optional
						                                   //'commentRss'   => 'the feed url of the associated comments', // optional
						                                   */ );
    	    			break;
    	    			
    	    		case 'gallery':
    	    			$image = $url['filename'];
    	    			$imageInfo = filemtime( $_SERVER['DOCUMENT_ROOT'] . '/public/images/gallery/data/' . $image );
						$url['title'] = ( is_null( $url['title'] ) ) ? __LANG_NO_TITLE__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 25 ) );
						$url['description'] = ( is_null( $url['title'] ) ) ? __LANG_NO_DESCRIPTION__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 35 ) );

				        $feedArray['entries'][$id]  = array('title'        => trim( strip_tags( "{$url['title']}: ". substr( $url['description'], 0, 32 ) ) ),
						                                   'link'         => "http://{$_SERVER['HTTP_HOST']}/browse/{$this->type}",
						                                   'description'  => "<h2>" .trim( strip_tags( $url['description'] ) ) . "</h2> <br /> <br /><img src='http://{$_SERVER['HTTP_HOST']}/public/images/{$this->type}/thumbs/$image' alt='Thumbnail' />", // only text, no html, required
						                                   'lastUpdate'   => $imageInfo, // optional
						                                   'pubDate'      => $imageInfo, // optional
						                                   'source'       => array(
						                                                           'title' => trim( strip_tags( "{$url['title']}" ) ), // required,
						                                                           'url' => "http://gallery/browse/gallery" // required
						                                                           ), // original source of the feed entry // optional
						                                   'guid'         => trim( "http://{$_SERVER['HTTP_HOST']}/public/images/gallery/data/$image?".time() ),
						                                   'content'      => trim( strip_tags( $url['description'] ) ) . '&nbsp;',
						                                   'enclosure'    => array( array( 'lenght' => 0, 'type' => 'image/jpeg', 'url' => "http://{$_SERVER['HTTP_HOST']}/public/images/{$this->type}/data/$image" ) )/*,
						                                   //'comments'     => 'comments page of the feed entry', // optional
						                                   //'commentRss'   => 'the feed url of the associated comments', // optional
						                                   */ );
    	    			break;
    	    			
    	    		case 'updates':
						$dt = explode( ' ', $url['update'] );
						$d  = explode( '-', $dt[0] );
						$t  = explode( ':', $dt[1] );
    	    			$ret = "<dl>";
						if ( $url['added_sources'] >= 1 ) $ret .= "<dd>{$url['added_sources']} " .__LANG_NEW_SOURCES__ . "</dd>";
						if ( $url['added_products'] >= 1 ) $ret .= "<dd><a href='http://{$_SERVER['HTTP_HOST']}/browse/products?q=ap&amp;ui={$url['id']}&amp;resetFilters=1' title='Vai agli articoli'>{$url['added_products']}</a> " . __LANG_NEW_PRODUCTS__ . "</dd>";
						if ( $url['changed_prices'] >= 1 ) $ret .= "<dd><a href='http://{$_SERVER['HTTP_HOST']}/browse/products?q=cpr&amp;ui={$url['id']}&amp;resetFilters=1' title='Vai agli articoli'>{$url['changed_prices']}</a> " . __LANG_NEW_PRICES__ . "</dd>";
						if ( $url['changed_products'] >= 1 ) $ret .= "<dd><a href='http://{$_SERVER['HTTP_HOST']}/browse/products?q=cp&amp;ui={$url['id']}&amp;resetFilters=1' title='Vai agli articoli'>{$url['changed_products']}</a> " . __LANG_CHANGED_PRODUCTS__ . '</dd>';
    	    			$ret .= "</dl>";
    	    			
    	    			$image = $url['filename'];
    	    	
						$url['title'] = date( 'd F, Y', mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ));

				        $feedArray['entries'][$id]  = array('title'        => trim( strip_tags( "{$url['update']}") ),
						                                   'link'         => "http://{$_SERVER['HTTP_HOST']}/browse/products?ui={$url['id']}&amp;resetFilters=1",
						                                   'description'  => "$ret", // only text, no html, required
						                                   'lastUpdate'   => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'pubDate'      => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'source'       => array(
						                                                           'title' => trim( strip_tags( "{$url['title']}" ) ), // required,
						                                                           'url' => "http://{$_SERVER['HTTP_HOST']}/browse/products?ui={$url['id']}&amp;resetFilters=1" // required
						                                                           ), // original source of the feed entry // optional
						                                   'guid'         => trim( "http://{$_SERVER['HTTP_HOST']}/browse/products?ui={$url['id']}&amp;resetFilters=1" ),
						                                   'content'      => trim( strip_tags( $url['description'] ) ) . '&nbsp;'/*
						                                   //'comments'     => 'comments page of the feed entry', // optional
						                                   //'commentRss'   => 'the feed url of the associated comments', // optional
						                                   */ );
    	    			break;
    	    			
    	    		case 'newsletters':
						$dt = explode( ' ', $url['date'] );
						$d  = explode( '-', $dt[0] );
						$t  = explode( ':', $dt[1] );
    	    			$image = $url['filename'];
    	    	
						$url['title'] = ( is_null( $url['title'] ) ) ? __LANG_NO_TITLE__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 25 ) );
						$url['description'] = ( is_null( $url['title'] ) ) ? __LANG_NO_DESCRIPTION__ : str_replace( "\n", '<br />', wordwrap( $url['title'], 35 ) );
		//				$url['creation_date'] = date( DATE_RFC822, $url['creation_date'] );
		//				
		//				print "CCC".$url['creation_date']."CCC";
				        $feedArray['entries'][$id]  = array('title'        => trim( strip_tags( mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ) . ": ". $url['title'] ) ),
						                                   'link'         => "http://{$_SERVER['HTTP_HOST']}/browse/products?".time(),
						                                   'description'  => "<h2>" .trim( strip_tags( $url['title'] ) ) . "</h2> <br /> <br />{$url['content_up']}<br /><br />{$url['products']}<br /><br />{$url['content_down']}", // only text, no html, required
						                                   'lastUpdate'   => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'pubDate'      => mktime( $t[0], $t[1], $t[2], $d[1], $d[2], $d[0] ), // optional
						                                   'source'       => array(
						                                                           'title' => trim( strip_tags( "{$url['title']}" ) ), // required,
						                                                           'url' => "http://{$_SERVER['HTTP_HOST']}/browse/products?".time() // required
						                                                           ), // original source of the feed entry // optional
						                                   'guid'         => "http://{$_SERVER['HTTP_HOST']}/browse/products?".time(),
						                                   'content'      => trim( strip_tags( $url['description'] ) ) . '&nbsp;'
/*,
						                                   //'comments'     => 'comments page of the feed entry', // optional
						                                   //'commentRss'   => 'the feed url of the associated comments', // optional
						                                   */ );
    	    			break;
    	    	}
    	    }
        }
        
    	return $feedArray; 
    }
    
    public function getFeedArray( )
    {
    	global $_SITE;

		$feedArray = array(
		      'title'       => $this->title,
		      'link'        => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'],
		      'charset'     => trim( $_SITE['config']['handler']->site->charset ),
		      'description' => 'Assemblaggio, Configurazione, Assistenza e Vendita apparati tecnologici e pc',
		      'author'      => trim( $_SITE['config']['handler']->site->admin ),
		      'email'       => trim( $_SITE['config']['handler']->site->admin ),
		      'webmaster'   => trim( $_SITE['config']['handler']->site->admin ),
		      'image'       => 'http://' . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . '/public/themes/default/images/logo.jpg',
		      'generator'   => trim( __GENERATOR__ ),
		      'language'    => trim( $_SITE['config']['env']['language']['short'] ), // optional
		      'ttl'         => '3600',
		      'copyright'   => '@'.date('Y').' Computer Shopping 3'
		      
		      //'rating'      => 'The PICS rating for the channel.',
		      /*'textInput'   => array(
		                             'title'       => 'the label of the Submit button in the text input area', // required,
		                             'description' => 'explains the text input area', // required
		                             'name'        => 'the name of the text object in the text input area', // required
		                             'link'        => 'the URL of the CGI script that processes text input requests', // required
		                             ),*/ // a text input box that can be displayed with the feed // optional, ignored if atom is used
		       );
		 return $feedArray;
    }
	
	public function getSitemap( $categoryAlso = TRUE )
	{
		global $_SITE;
		//_dump($_SITE);
		$links = $_SITE['database']['handler']->select( )
		                                     ->from( 'sitemap' )
		                                     ->order( array( 'lastmod DESC', 'priority DESC' ) );
		$handle = $links->query( );
		$links = $handle->fetchAll( );

		foreach( $links as $link )
			if ( ! @in_array( $linkz[$link['controller']], $link['action'] ) )
				$linkz[$link['controller']][$link['action']] = $link;
				
		foreach( $links as $link )
			if ( ! @in_array( $link['controller'], $links_controllers ) )
				$links_controllers[] = $link['controller'];

		foreach( $links as $link )
			if ( ! @in_array( $link['action'], $actions[$link['controller']] ) )
			$actions[$link['controller']][] = $link['action'];

//			_dump($links_controllers);
//			_dump($actions);
			
    	$host = ( $_SERVER['SERVER_PORT'] == '80' ) 
    	      ? $_SERVER['HTTP_HOST'] : "{$_SERVER['HTTP_HOST']}:{$_SERVER['SERVER_PORT']}";

		foreach ( glob( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . '*' ) as $controller )
		{
			$controller = str_replace( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR, NULL, $controller );
			//_dump('C'.$controller);
			// *** Check if the controller exists on database
			if ( @in_array( $controller, $links_controllers ) !== FALSE )
			{
				foreach( glob( $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . 
				               $controller . DIRECTORY_SEPARATOR . '*' ) as $action )
				{
					$patterns[0] = '/'.str_replace( '/', '\\/', $_SITE['config']['fs']['path_view'] . DIRECTORY_SEPARATOR . 
					                                            $controller . DIRECTORY_SEPARATOR ).'/';
					$patterns[1] = '/.phtml/';
					$replacements[0] = NULL;
					$replacements[1] = NULL;
					$action = preg_replace( $patterns, $replacements, $action );
//					_dump($action);
					// *** Check if the action exists on database				
					if ( @in_array( $action, $actions[$controller] ) !== FALSE ) 
					{
					    	unset( $url );
					    	if ( $action != 'index' ) $url = "$controller/$action";
					    	elseif ( $controller != 'index' && $action == 'index' ) $url = $controller;
//					    	_dump($linkz[$controller][$action]);
							$linkz[$controller][$action]['lastmod'] = array_pop( array_reverse( explode( ' ', $linkz[$controller][$action]['lastmod'] ) ) );
							
					    	if ( $linkz[$controller][$action]['published'] == 'Y' && ! @array_search( "http://$host/$url", $urls[$controller] ) )
					    	$urls[$controller][] = array( 'loc' => "http://$host/$url", 
					    	                              'lastmod' => date( 'Y-m-' ) . ( date( 'd' ) - 2 ),//$linkz[$controller][$action]['lastmod'], 
					    	                              'changefreq' => $linkz[$controller][$action]['changefreq'], 
					    	                              'priority' => $linkz[$controller][$action]['priority'] );
					    	                                 
					    // *** If not present we create a new database entry
					} else $_SITE['database']['handler']->insert( 'sitemap', array( 'controller' => $controller, 'action' => $action ) );
				}
				// *** If not present we create a new database entry
			} else $_SITE['database']['handler']->insert( 'sitemap', array( 'controller' => $controller, 'action' => 'index' ) );
		}
		
		return $urls;
	}
} 