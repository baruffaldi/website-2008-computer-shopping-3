<?php

class BFCMS_Helper
{
	public function galleryGetImage( $image, $thumb = FALSE, $stream = FALSE, $print = FALSE, $random = FALSE, $product = FALSE )
	{
		global $_SITE;
		
		if ( $random ) 
		{
			$q = $_SITE['database']['handler']->select( )
			                                  ->from( 'gallery', array( 'total' => 'COUNT(*)' ) )
			                                  ->where( 'status = ?', 'Y' );
			                             
			$h = $q->query( );
			$total = $h->fetchAll( );
			
			$rand = rand( 1, $total[0]['total'] );
			
			if ( $rand <= 1 || $rand == $total[0]['total'] ) $rand = NULL;
			
			$q = $_SITE['database']['handler']->select( )
			                                  ->from( 'gallery' )
			                                  ->where( 'status = ?', 'Y' )
			                                  ->limit( 1, $rand );
			                             
			$h = $q->query( );
			$image = $h->fetchAll( );
			
			$image = $image[0]['filename'];
		}
		
		if ( $product ) {
			$gallery = 'seller';
			$i = $image;
			if ( ! file_exists( $_SERVER['DOCUMENT_ROOT']."/public/images/$gallery/data/" . md5( $image[1] ) . ".jpg" ) )
			{
				file_put_contents( $_SERVER['DOCUMENT_ROOT']."/public/images/$gallery/data/" . md5( $image[1] ) . ".jpg", file_get_contents( 'http://www.brevi.it/common/images/iis_images/400x400/' . $image[0] .".gif" ) );
				$filename = "/public/images/$gallery/data/" . md5( $image[1] ) . ".jpg";
				
			} else $filename = 'http://www.brevi.it/common/images/iis_images/400x400/' . $image[0] .".gif";

			$image = ( $thumb ) ? "/public/images/$gallery/thumbs/" . md5( $image[1] ) . ".jpg"
			       	            : "/public/images/$gallery/data/" . md5( $image[1] ) . ".jpg";
		} else {
			$gallery = 'gallery';
			$filename = $_SERVER['DOCUMENT_ROOT'] . "/public/images/$gallery/data/$image";
			$image = ( $thumb ) ? "/public/images/$gallery/thumbs/$image" 
			       	            : $filename;
		}
		
		if ( $thumb )
		{
			if ( ! file_exists( $_SERVER['DOCUMENT_ROOT'] . $image ) )
			{
								$t1 = new ImageBatchTransformation();
								$t1->source = $filename;
								$t1->destination = $_SERVER['DOCUMENT_ROOT'] . $image;
								$t1->maxWidth = 180;
								$t1->maxHeight = 150;
								$t1->format = TI_JPEG;
								$t1->jpegQuality = 95;
								$t1->interlace = true;
								$t1->replaceExisted = false;
								$t1->label['text'] = date('y') . ' (c) computerShopping3.it';
								$t1->label['vertPos'] = TI_POS_BOTTOM;
								$t1->label['horzPos'] = TI_POS_RIGHT;
								$t1->label['font'] = $_SITE['config']['fs']['path_public'] . DIRECTORY_SEPARATOR . 'FREESCPT.TTF';
								$t1->label['size'] = 10;
								$t1->label['color'] = '#ffff00';
								$t1->label['angle'] = 0;
								$t1->label['rotateAngle'] = 0;
								$t1->label['rotateBgColor'] = '#ffffff';
                              
                              $ibp = new ImageBatchProcessor();
                              
                              $n = $ibp->processEx(array($t1));

								if ( file_exists( $_SERVER['DOCUMENT_ROOT'] . $image ) && $ibp->thumbnail->NOTCREATED != TRUE && $gallery == 'seller' )
									 $_SITE['database']['handler']->update( 'products', array( 'thumb' => 'Y' ), "id_product = {$i[1]}" );
								else $_SITE['database']['handler']->update( 'products', array( 'thumb' => 'N' ), "id_product = {$i[1]}" );
			}

			if ( ! file_exists( $_SERVER['DOCUMENT_ROOT'] . $image ) ) $image = "http://www.difossombrone.it/images/genealogie/foto_non_disponibile.gif";
			else $image = $_SERVER['DOCUMENT_ROOT'] . $image;

			if ( $stream )
				if ( $print )
				{
					print file_get_contents( $image );
					header( 'Content-type: image/' . array_pop( explode( '.', $image ) ) );
				} else return file_get_contents( $image );
			else 
				if ( $print ) print str_replace( $_SERVER['DOCUMENT_ROOT'], NULL, $image );
				else return str_replace( $_SERVER['DOCUMENT_ROOT'], NULL, $image );
			
		} else {

			if ( $stream )
				if ( $print )
				{
					print file_get_contents( $image );
					header( 'Content-type: image/' . array_pop( explode( '.', $image ) ) );
				} else return file_get_contents( $image );
			else 
				if ( $print ) print str_replace( $_SERVER['DOCUMENT_ROOT'], NULL, $image );
				else return str_replace( $_SERVER['DOCUMENT_ROOT'], NULL, $image );
		}
	}
	
	public function getVars( $without = NULL )
	{
		$ret = NULL;
		if ( is_null( $without ) )
		{
			foreach( $_GET as $k => $v )
				$ret .= "&amp;$k=$v";
		} else {
			$without = array_merge( $without, array( 'updateFilter' ) );
			foreach( $_GET as $k => $v )
				if ( ! in_array( $k, $without ) )
					$ret .= "&amp;$k=$v";
		}
		
		return $ret;
	}

	public function removeAccents( $str )
	{
		return strtr( $str,
		              "���������������������������������������������������������������������",
                      "SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy" );
	}

	public function renderTable( $data, $title = NULL, $render_paging = TRUE, $render_title = TRUE, $render_headers = TRUE, $css = array( ), $fields_mapping = array(  ), $hide_fields = array( ), $type = 'table' )
	{
		$css_title = ( array_key_exists( 'title', $css ) ) ? " {$css['title']}" : NULL;
		$css_table = ( array_key_exists( 'table', $css ) ) ? " {$css['table']}" : NULL;
		$css_alter = ( array_key_exists( 'alter', $css ) ) ? " {$css['alter']}" : NULL;
		$css_page  = ( array_key_exists( 'page', $css ) ) ? " {$css['page']}" : NULL;
		$css_tr    = ( array_key_exists( 'tr', $css ) ) ? " {$css['tr']}" : NULL;
		$css_th    = ( array_key_exists( 'th', $css ) ) ? " {$css['th']}" : NULL;
		$css_td    = ( array_key_exists( 'td', $css ) ) ? " {$css['td']}" : NULL;
		$css_dl    = ( array_key_exists( 'dl', $css ) ) ? " {$css['dl']}" : NULL;
		$css_dt    = ( array_key_exists( 'dt', $css ) ) ? " {$css['dt']}" : NULL;
		$css_dd    = ( array_key_exists( 'dd', $css ) ) ? " {$css['dd']}" : NULL;
		
		$cols = count( $data[0] ) - count( $hide_fields );	

		switch( $type )
		{
			default:
			case 'table':
				$html = "<table$css_table>";
				
				if ( $render_title )
				{
					$html .= "<tr>";
					$html .= "<th$css_title colspan=\"$cols\">$title</th>";
					$html .= "</tr>";
				}
				
				if ( $render_headers )
				{
					$html .= "<tr$css_th>";
					$headers = array_keys( $data[0] );
					foreach ( $headers as $header )
					{
						if ( ! is_int( array_search( $header, $hide_fields ) ) )
						{
							$header = ( array_key_exists( $header, $fields_mapping ) ) ? $fields_mapping[$header] : $header;
							$html .= "<th>$header</th>";
						}
					}
					$html .= "</tr>";
				}
				
				foreach ( $data as $k => $d )
				{
					if ( is_float( $k / 2 ) ) $html .= "<tr$css_tr>";
					else $html .= "<tr$css_alter>";
							
					foreach ( $d as $k => $value ) 
					{
						if ( ! is_int( array_search( $k, $hide_fields ) ) ) 
						    if ( substr( $value, ( strlen( $value ) -1 ) ) == '.' ) $html .= '<td>' . substr( $value, 0, -1 ) . '</td>';
						    else $html .= "<td>$value</td>";
					}
					
					$html .= "</tr>";
				}
				
				if ( $render_paging )
				{
					$html .= "<tr$css_page>";
					$html .= "<th colspan=\"$cols\"><a href=\"\" title=\"Previous\">Previous</a> :: <a href=\"\" title=\"Next\">Next</a></th>";
					$html .= "</tr>";
				}
				
				$html .= "</table>";
				break;
				
			case 'list':
				$html = "<dl$css_dl>";
				
				if ( $render_title )
					$html .= "<dt$css_title>$title</dt>";
				
				foreach ( $data as $k => $d )
				{
					if ( is_float( $k / 2 ) ) $html .= "<dd$css_dd>";
					else $html .= "<dd$css_alter>";
					
					foreach ( $d as $k => $value ) 
					{
						if ( ! is_int( array_search( $k, $hide_fields ) ) ) 
						$html .= "$value";
					}
					
					$html .= "</dd>";
				}
				
				if ( $render_paging )
					$html .= "<dt$css_page><a href=\"\" title=\"Previous\">Previous</a> :: <a href=\"\" title=\"Next\">Next</a></dt>";
				
				$html .= "</dl>";
				break;
		}
		
		return $html;
	}

	public function cleanUriGet()
	{
		$return = NULL;
		$first = TRUE;
		
		foreach( $_GET as $key => $value )
		{
			if ( substr( $key, 0, 4 ) != 'page' )
			switch( $key )
			{
				default:
					if ( ! $first ) $return .= "&";
					$return .= $key;
					if ( ! empty( $value ) ) $return .= "=";
					$return .= $value;
					$first = FALSE;
					break;
			}
		}
		
		return $return;
	}
	
	public function getToken( $lenght = 10 )
	{
		while( strlen( $token ) <= $lenght )
		{		
			$subsets[1] = array('min' => 48, 'max' => 57); // ascii numbers
			$subsets[2] = array('min' => 97, 'max' => 122); // ascii lowercase English letters
			//$subsets[3] = array('min' => 65, 'max' => 90); // ascii uppercase English letters
			
			$s = rand( 1, count( $subsets ) );
			
			$ascii_code = rand($subsets[$s]['min'], $subsets[$s]['max']);
			
			// Little hack to keep the scheme "for each one integer, one alpha".
			//if ( is_int( chr( $ascii_code ) ) && is_int( $old_ascii_code ) || ! is_int( chr( $ascii_code ) ) && ! is_int( $old_ascii_code ) ) $ascii_code = NULL;
			// Little hack to avoid double characters
			if ( chr( $ascii_code ) == $old_ascii_code ) $ascii_code = NULL;
			
			if ( ! is_null( $ascii_code ) ) 
			{
				$token .= chr( $ascii_code );
				$old_ascii_code = chr( $ascii_code );
			}
		}
		
		return $token;
	}
}







/**
* ImageBatchProcessor class, version 1.0 (for PHP >= 4.3)
* (c) 2008 Vagharshak Tozalakyan <vagh{at}tozalakyan{dot}com>
*
* This class can be used to perform different kind of operations over a group
* of images. It can proportionally resize images (create thumbnails), rotate
* images, convert between different image formats, append textual labels and/or
* small graphic watermarks to images, etc.
*
* Transformation parameters may be applied to all images in a directory or
* separately to each image in a set - the images in source or destination sets
* may be located in the same or different directories.
*
* The class also contains a traversal method which walks through a directory
* and calls a callback function for each item. The method may be used, for
* example, to rename all images (or other files) in a directory using
* predefined naming template (photo01.jpg, photo02.jpg, ...).
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*
* @version  1.0
* @author   Vagharshak Tozalakyan <vagh{at}tozalakyan{dot}com>
* @license  http://www.opensource.org/licenses/mit-license.php
*/

define('IBP_IMAGE_REGEXP', '/^(.*)(\.jpg|\.jpeg|\.gif|\.png)$/is');
define('IBP_ALL_REGEXP', '/^(.*)$/is');

define('TI_MAX_IMG_SIZE', 100000);

define('TI_JPEG', 'image/jpeg');
define('TI_PNG', 'image/png');
define('TI_GIF', 'image/gif');

define('TI_INTERLACE_OFF', 0);
define('TI_INTERLACE_ON', 1);

define('TI_STDOUT', '');
define('TI_RESOURCE', '-1000');

define('TI_NO_LOGO', '');
define('TI_NO_LABEL', '');

define('TI_POS_LEFT', 0);
define('TI_POS_RIGHT', 1);
define('TI_POS_CENTER', 2);
define('TI_POS_TOP', 3);
define('TI_POS_BOTTOM', 4);

class ThumbnailImage
{
    var $srcFile = '';
    var $destFile = TI_STDOUT;
    var $destType = TI_JPEG;
    var $interlace = TI_INTERLACE_OFF;
    var $jpegQuality = -1;
    var $maxWidth = 100;
    var $maxHeight = 100;
    var $fitToMax = false;
    var $logo = array();
    var $label = array();
    var $rotateAngle = 0;
    var $rotateBgColor = '#ffffff';
    var $srcWidth = 0;
    var $srcHeight = 0;

    function ThumbnailImage($srcFile = '')
    {
        $this->srcFile = $srcFile;
        $this->logo['file'] = TI_NO_LOGO;
        $this->logo['vertPos'] = TI_POS_TOP;
        $this->logo['horzPos'] = TI_POS_LEFT;
        $this->label['text'] = TI_NO_LABEL;
        $this->label['vertPos'] = TI_POS_BOTTOM;
        $this->label['horzPos'] = TI_POS_RIGHT;
        $this->label['font'] = '';
        $this->label['size'] = 20;
        $this->label['color'] = '#000000';
        $this->label['angle'] = 0;
    }

    function parseColor($hexColor)
    {
        if (strpos($hexColor, '#') === 0) {
            $hexColor = substr($hexColor, 1);
        }
        $r = hexdec(substr($hexColor, 0, 2));
        $g = hexdec(substr($hexColor, 2, 2));
        $b = hexdec(substr($hexColor, 4, 2));
        return array ($r, $g, $b);
    }

    function getImageStr($imageFile)
    {
        if (function_exists('file_get_contents')) {
            $str = @file_get_contents($imageFile);
            if (!$str) {
                $err = sprintf('Failed reading image data from <b>%s</b>', $imageFile);
                //trigger_error($err, E_USER_ERROR);
            }
            return $str;
        }
        $f = fopen($imageFile, 'rb');
        if (!$f) {
            $err = sprintf('Failed reading image data from <b>%s</b>', $imageFile);
            //trigger_error($err, E_USER_ERROR);
        }
        $fsz = @filesize($imageFile);
        if (!$fsz) {
            $fsz = TI_MAX_IMG_SIZE;
        }
        $str = fread($f, $fsz);
        fclose ($f);
        return $str;
    }

    function loadImage($imageFile, &$imageWidth, &$imageHeight)
    {
        $imageWidth = 0;
        $imageHeight = 0;
        $imageData = $this->getImageStr($imageFile);

        $image = imagecreatefromstring($imageData);
        if (!$image) {
        	$this->NOTCREATED = TRUE;
        	$image = imagecreatefromstring( file_get_contents( "http://www.difossombrone.it/images/genealogie/foto_non_disponibile.gif" ));
            //$err = sprintf('Cannot create the copy of <b>%s</b>', $imageFile);
            //trigger_error($err, E_USER_ERROR);
        }
        if ($this->rotateAngle && function_exists('imagerotate')) {
            list($r, $g, $b) = $this->parseColor($this->rotateBgColor);
            $bgColor = imagecolorallocate($image, $r, $g, $b);
            $image = imagerotate($image, $this->rotateAngle, $bgColor);
        }
        $imageWidth = imagesx($image);
        $imageHeight = imagesy($image);

        $this->srcWidth = $imageWidth;
        $this->srcHeight = $imageHeight;
        return $image;

    }

    function getThumbSize($srcWidth, $srcHeight)
    {
        $maxWidth = $this->maxWidth;
        $maxHeight = $this->maxHeight;
        $xRatio = $maxWidth / $srcWidth;
        $yRatio = $maxHeight / $srcHeight;
        $isSmall = ($srcWidth <= $maxWidth && $srcHeight <= $maxHeight);
        if (!$this->fitToMax && $isSmall) {
            $destWidth = $srcWidth;
            $destHeight = $srcHeight;
        } elseif ($xRatio * $srcHeight < $maxHeight) {
            $destWidth = $maxWidth;
            $destHeight = ceil($xRatio * $srcHeight);
        } else {
            $destWidth = ceil($yRatio * $srcWidth);
            $destHeight = $maxHeight;
        }
        return array ($destWidth, $destHeight);
    }

    function addLogo($thumbWidth, $thumbHeight, &$thumbImg)
    {
        extract($this->logo);
        $logoImage = $this->loadImage($file, $logoWidth, $logoHeight);
        if ($vertPos == TI_POS_CENTER) {
            $yPos = ceil($thumbHeight / 2 - $logoHeight / 2 );
        } elseif ($vertPos == TI_POS_BOTTOM) {
            $yPos = $thumbHeight - $logoHeight;
        } else {
            $yPos = 0;
        }
        if ($horzPos == TI_POS_CENTER) {
            $xPos = ceil($thumbWidth / 2 - $logoWidth / 2);
        } elseif ($horzPos == TI_POS_RIGHT) {
            $xPos = $thumbWidth - $logoWidth;
        } else {
            $xPos = 0;
        }
        if (!imagecopy($thumbImg, $logoImage, $xPos, $yPos, 0, 0, $logoWidth, $logoHeight)) {
            trigger_error('Cannot copy the logo image', E_USER_ERROR);
        }
    }

    function addLabel($thumbWidth, $thumbHeight, &$thumbImg)
    {
        extract($this->label);
        list($r, $g, $b) = $this->parseColor($color);
        $colorId = imagecolorallocate($thumbImg, $r, $g, $b);
        $textBox = imagettfbbox($size, $angle, $font, $text);
        $textWidth = $textBox[2] - $textBox[0];
        $textHeight = abs($textBox[1] - $textBox[7]);
        if ($vertPos == TI_POS_TOP) {
            $yPos = 5 + $textHeight;
        } elseif ($vertPos == TI_POS_CENTER) {
            $yPos = ceil($thumbHeight / 2 - $textHeight / 2);
        } elseif ($vertPos == TI_POS_BOTTOM) {
            $yPos = $thumbHeight - $textHeight;
        }
        if ($horzPos == TI_POS_LEFT) {
            $xPos = 5;
        } elseif ($horzPos == TI_POS_CENTER) {
            $xPos = ceil($thumbWidth / 2 - $textWidth / 2);
        } elseif ($horzPos == TI_POS_RIGHT) {
            $xPos = $thumbWidth - $textWidth - 5;
        }
        imagettftext($thumbImg, $size, $angle, $xPos, $yPos, $colorId, $font, $text);
    }

    function outputThumbImage($destImage)
    {
        imageinterlace($destImage, $this->interlace);
        header('Content-type: ' . $this->destType);
        if ($this->destType == TI_JPEG) {
            imagejpeg($destImage, '', $this->jpegQuality);
        } elseif ($this->destType == TI_GIF) {
            imagegif($destImage);
        } elseif ($this->destType == TI_PNG) {
            imagepng($destImage);
        }
    }

    function saveThumbImage($imageFile, $destImage)
    {
        imageinterlace($destImage, $this->interlace);
        if ($this->destType == TI_JPEG) {
            imagejpeg($destImage, $this->destFile, $this->jpegQuality);
        } elseif ($this->destType == TI_GIF) {
            imagegif($destImage, $this->destFile);
        } elseif ($this->destType == TI_PNG) {
            imagepng($destImage, $this->destFile);
        }
    }

    function output()
    {
        $srcImage = $this->loadImage($this->srcFile, $srcWidth, $srcHeight);
        $destSize = $this->getThumbSize($srcWidth, $srcHeight);
        $destWidth = $destSize[0];
        $destHeight = $destSize[1];
        $destImage = imagecreatetruecolor($destWidth, $destHeight);
        if (!$destImage) {
            //trigger_error('Cannot create final image', E_USER_ERROR);
        }
        imagecopyresampled($destImage, $srcImage, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
        if ($this->logo['file'] != TI_NO_LOGO) {
            $this->addLogo($destWidth, $destHeight, $destImage);
        }
        if ($this->label['text'] != TI_NO_LABEL) {
            $this->addLabel($destWidth, $destHeight, $destImage);
        }
        if ($this->destFile == TI_STDOUT) {
            $this->outputThumbImage($destImage);
        } elseif ($this->destFile == TI_RESOURCE) {
            imagedestroy($srcImage);
            return $destImage;
        } else {
            $this->saveThumbImage($this->destFile, $destImage);
        }
        imagedestroy($srcImage);
        imagedestroy($destImage);
    }

} 

class ImageBatchTransformation
{
    var $source = '';
    var $destination = '';
    var $format = TI_JPEG;
    var $jpegQuality = -1;
    var $interlace = TI_INTERLACE_OFF;
    var $maxWidth = 800;
    var $maxHeight = 600;
    var $fitToMax = false;
    var $logo = array();
    var $label = array();
    var $rotateAngle = 0;
    var $rotateBgColor = '#ffffff';
    var $replaceExisted = true;
 
    function ImageBatchTransformation()
    {
        $this->logo['file'] = TI_NO_LOGO;
        $this->logo['vertPos'] = TI_POS_TOP;
        $this->logo['horzPos'] = TI_POS_LEFT;
        $this->label['text'] = TI_NO_LABEL;
        $this->label['vertPos'] = TI_POS_BOTTOM;
        $this->label['horzPos'] = TI_POS_RIGHT;
        $this->label['font'] = '';
        $this->label['size'] = 20;
        $this->label['color'] = '#000000';
        $this->label['angle'] = 0;
    }

}

class ImageBatchProcessor
{
    var $thumbnail = null;
    var $extensions = array(TI_JPEG => '.jpg', TI_GIF => '.gif', TI_PNG => '.png');

    function ImageBatchProcessor()
    {
        $this->transform = new ImageBatchTransformation();
        $this->thumbnail = new ThumbnailImage();
    }

    function applyTransformation($transformObj, &$thumbnailObj)
    {
        $thumbnailObj->srcFile = $transformObj->source;
        $thumbnailObj->destFile = $transformObj->destination;
        $thumbnailObj->destType = $transformObj->format;
        $thumbnailObj->interlace = $transformObj->interlace;
        $thumbnailObj->jpegQuality = $transformObj->jpegQuality;
        $thumbnailObj->maxWidth = $transformObj->maxWidth;
        $thumbnailObj->maxHeight = $transformObj->maxHeight;
        $thumbnailObj->fitToMax = $transformObj->fitToMax;
        $thumbnailObj->logo = $transformObj->logo;
        $thumbnailObj->label = $transformObj->label;
        $thumbnailObj->rotateAngle = $transformObj->rotateAngle;
        $thumbnailObj->rotateBgColor = $transformObj->rotateBgColor;
    }

    function normalizePath($path)
    {
        $path = str_replace('\\', '/', trim($path));
        if (!empty($path) && substr($path, -1) != '/') {
            $path .= '/';
        }
        return $path;
    }

    function process($transformObj, $filter = IBP_IMAGE_REGEXP, $count = 0)
    {
        $srcDir = $this->normalizePath($transformObj->source);
        $destDir = $this->normalizePath($transformObj->destination);
        $this->applyTransformation($transformObj, $this->thumbnail);
        if (!($dir = opendir($srcDir))) {
            return false;
        }
        $i = 0;
        while (false !== ($f = readdir($dir))) {
            if ($f == '.' || $f == '..' || !preg_match($filter, $f)) {
                continue;
            }
            $ext = substr($f, strrpos($f, '.'));
            if ($count <= 0 || $i < $count) {
                $this->thumbnail->srcFile = $srcDir . $f;
                $this->thumbnail->destFile = $destDir . basename($f, $ext) . $this->extensions[$transformObj->format];
                if ($transformObj->replaceExisted || !file_exists($this->thumbnail->destFile)) {
                    $this->thumbnail->output();
                    $i++;
                }
            }
        }
        closedir($dir);
        return $i;
    }

    function processEx($transformArray)
    {
        foreach ($transformArray as $i => $transformObj) {
            $this->applyTransformation($transformObj, $this->thumbnail);
            if ($transformObj->replaceExisted || !file_exists($transformObj->destination)) {
                $this->thumbnail->output();
            }
        }
        
        
        return $i + 1;
    }

    function dirWalk($path, $callback, $filter = IBP_ALL_REGEXP)
    {
        $path = $this->normalizePath($path);
        $files = array();
        if (!($dir = opendir($path))) {
            return false;
        }
        while (false !== ($f = readdir($dir))) {
            if ($f == '.' || $f == '..' || !preg_match($filter, $f)) {
                continue;
            }
            $ext = substr($f, strrpos($f, '.'));
            $files[] = array(basename($f, $ext), $ext);
        }
        closedir($dir);
        $numFiles = sizeof($files);
        $l = strlen(strval($numFiles));
        foreach ($files as $i => $file) {
            $oldName = $file[0] . $file[1];
            $padded = str_pad(strval($i + 1), $l, '0', STR_PAD_LEFT);
            $newName = $callback($path, $i, $padded, $oldName, $file[0], $file[1]);
        }
        return $numFiles;
    }

} 


//
// +-----------------------------------+
// |        Image Filter v 1.0         |
// |      http://www.SysTurn.com       |
// +-----------------------------------+
//
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the ISLAMIC RULES and GNU Lesser General Public
//   License either version 2, or (at your option) any later version.
//
//   ISLAMIC RULES should be followed and respected if they differ
//   than terms of the GNU LESSER GENERAL PUBLIC LICENSE
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the license with this software;
//   If not, please contact support @ S y s T u r n .com to receive a copy.
//

class ImageFilter
{                              #R  G  B
    var $colorA = 7944996;     #79 3B 24
    var $colorB = 16696767;    #FE C5 BF


    var $arA = array();
    var $arB = array();
    
    function ImageFilter()
    {
        $this->arA['R'] = ($this->colorA >> 16) & 0xFF;
        $this->arA['G'] = ($this->colorA >> 8) & 0xFF;
        $this->arA['B'] = $this->colorA & 0xFF;
        
        $this->arB['R'] = ($this->colorB >> 16) & 0xFF;
        $this->arB['G'] = ($this->colorB >> 8) & 0xFF;
        $this->arB['B'] = $this->colorB & 0xFF;
    }
    
    function GetScore($image)
    {
        $x = 0; $y = 0;
        $img = $this->_GetImageResource($image, $x, $y);
        if(!$img) return false;

        $score = 0;
        
        $xPoints = array($x/8, $x/4, ($x/8 + $x/4), $x-($x/8 + $x/4), $x-($x/4), $x-($x/8));
        $yPoints = array($y/8, $y/4, ($y/8 + $y/4), $y-($y/8 + $y/4), $y-($y/8), $y-($y/8));
        $zPoints = array($xPoints[2], $yPoints[1], $xPoints[3], $y);

        
        for($i=1; $i<=$x; $i++)
        {
            for($j=1; $j<=$y; $j++)
            {
                $color = imagecolorat($img, $i, $j);
                if($color >= $this->colorA && $color <= $this->colorB)
                {
                    $color = array('R'=> ($color >> 16) & 0xFF, 'G'=> ($color >> 8) & 0xFF, 'B'=> $color & 0xFF);
                    if($color['G'] >= $this->arA['G'] && $color['G'] <= $this->arB['G'] && $color['B'] >= $this->arA['B'] && $color['B'] <= $this->arB['B'])
                    {
                        if($i >= $zPoints[0] && $j >= $zPoints[1] && $i <= $zPoints[2] && $j <= $zPoints[3])
                        {
                            $score += 3;
                        }
                        elseif($i <= $xPoints[0] || $i >=$xPoints[5] || $j <= $yPoints[0] || $j >= $yPoints[5])
                        {
                            $score += 0.10;
                        }
                        elseif($i <= $xPoints[0] || $i >=$xPoints[4] || $j <= $yPoints[0] || $j >= $yPoints[4])
                        {
                            $score += 0.40;
                        }
                        else
                        {
                            $score += 1.50;
                        }
                    }
                }
            }
        }
        
        imagedestroy($img);
        
        $score = sprintf('%01.2f', ($score * 100) / ($x * $y));
        if($score > 100) $score = 100;
        return $score;
    }
    
    function GetScoreAndFill($image, $outputImage)
    {
        $x = 0; $y = 0;
        $img = $this->_GetImageResource($image, $x, $y);
        if(!$img) return false;

        $score = 0;

        $xPoints = array($x/8, $x/4, ($x/8 + $x/4), $x-($x/8 + $x/4), $x-($x/4), $x-($x/8));
        $yPoints = array($y/8, $y/4, ($y/8 + $y/4), $y-($y/8 + $y/4), $y-($y/8), $y-($y/8));
        $zPoints = array($xPoints[2], $yPoints[1], $xPoints[3], $y);


        for($i=1; $i<=$x; $i++)
        {
            for($j=1; $j<=$y; $j++)
            {
                $color = imagecolorat($img, $i, $j);
                if($color >= $this->colorA && $color <= $this->colorB)
                {
                    $color = array('R'=> ($color >> 16) & 0xFF, 'G'=> ($color >> 8) & 0xFF, 'B'=> $color & 0xFF);
                    if($color['G'] >= $this->arA['G'] && $color['G'] <= $this->arB['G'] && $color['B'] >= $this->arA['B'] && $color['B'] <= $this->arB['B'])
                    {
                        if($i >= $zPoints[0] && $j >= $zPoints[1] && $i <= $zPoints[2] && $j <= $zPoints[3])
                        {
                            $score += 3;
                            imagefill($img, $i, $j, 16711680);
                        }
                        elseif($i <= $xPoints[0] || $i >=$xPoints[5] || $j <= $yPoints[0] || $j >= $yPoints[5])
                        {
                            $score += 0.10;
                            imagefill($img, $i, $j, 14540253);
                        }
                        elseif($i <= $xPoints[0] || $i >=$xPoints[4] || $j <= $yPoints[0] || $j >= $yPoints[4])
                        {
                            $score += 0.40;
                            imagefill($img, $i, $j, 16514887);
                        }
                        else
                        {
                            $score += 1.50;
                            imagefill($img, $i, $j, 512);
                        }
                    }
                }
            }
        }
        imagejpeg($img, $outputImage);

        imagedestroy($img);

        $score = sprintf('%01.2f', ($score * 100) / ($x * $y));
        if($score > 100) $score = 100;
        return $score;
    }
    
    function _GetImageResource($image, &$x, &$y)
    {
        $info = GetImageSize($image);
        
        $x = $info[0];
        $y = $info[1];
        
        switch( $info[2] )
        {
            case IMAGETYPE_GIF:
                return @ImageCreateFromGif($image);
                
            case IMAGETYPE_JPEG:
                return @ImageCreateFromJpeg($image);
                
            case IMAGETYPE_PNG:
                return @ImageCreateFromPng($image);
                
            default:
                return false;
        }
    }
} 